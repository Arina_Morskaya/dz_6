import requests
import os
import zipfile
import itertools
from itertools import combinations_with_replacement
import string

r = zipfile.ZipFile('D:/lesson6.zip', 'r')
password_list = []
os.mkdir('D:/DZ_6')
user_requests = {}

for i in itertools.combinations_with_replacement(string.ascii_letters.lower(), 3):
    password_list.append(''.join(i))
for i in password_list:
    try:
        r.extractall('D:/', pwd=i.encode())
    except: pass


def reading_file(original_file):
    for line in original_file.readlines():
        column = line.split('\t')
        if user_requests.get(column[3]) is None:
            user_requests[column[3]] = {column[5]: [column[4]]}
        else:
            if user_requests[column[3]].get(column[5]) is None:
                user_requests[column[3]][column[5]] = [column[4]]
            else:
                user_requests[column[3]][column[5]].append(column[4])


for i in os.walk('D:/lesson6'):
    for j in i[2]:
        with open(os.path.join(i[0], j), errors="ignore") as f:
            reading_file(f)


for key, value in user_requests.items():
    if key.replace(' ', '').isalpha():
        city_name = '{}{}'.format(key.lower().replace(' ', '_'), '.tsv')
    else:
        city_name = 'unknown_city.tsv'

    for k, v in user_requests[key].items():
        with open(os.path.join('D:/DZ_6', city_name), 'a') as city_file:
            city_file.write('{}{}{}{}'.format(k, r'\t', len(set(v)), '\n'))
